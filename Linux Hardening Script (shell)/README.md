**Introduction**: 

This Linux shell script aims to harden a centOS/RHEL server. It performs the following tasks:

* Updates all packages
* Secures SSH with a new sshd_config file
* Installs chkrootkit. This checks operating system binaries for rootkit modifications
* Installs Fail2ban to prevent brute force attempts using IPtables
* Installs CSF. Config Server Firewall (CSF) is a Stateful Packet Inspection (SPI) firewall, Login/Intrusion Detection and     Security application for Linux servers. The CSF configuration is tweaked in the hardening script using a custom csf.conf file
* Installs sysstat for load monitoring
* Disables and stops unwanted services

**Working**:

1. We first check if the user has root privelges, if not the script will exit
2. The script is modularized into functions for flexibility. A particular task can be excluded just by not calling the function. A lot more tasks can be added
3. The script has error checking conditions
4. Uploads tweaked csf.conf, sshd_config from the cwd

**Improvement scope**:

* Script can be converted to an ansible playbook
* Kernel upgrades can be performed 
