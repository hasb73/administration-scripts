#!/bin/bash


###Checking if the user is Root
rootcheck() {
if [ $EUID -ne 0 ];then
      echo "This script should be run by root only. EXITING"
     
else
    echo "Welcome you are root with uid: $EUID"
fi
}


###Invoking Rootcheck Function####
rootcheck



####Creating a continue prompt Function######
function should_continue() {
    read -p "Countinue?: y/n: " RESPONSE
    if [ "$RESPONSE" == "n" ]; then
        exit
    fi
    echo " "
}

should_continue


#######Performing Yum Update########

yum_update(){
    echo "Performing Yum Update"
    echo -e "\e[34m---------------------------------------------------------------------------------------------------------\e[00m"
    echo -e "\e[93m[+]\e[00m Updating the System"
    echo -e "\e[34m---------------------------------------------------------------------------------------------------------\e[00m"
    
    ####Error condition#####
    yum -y update>yum_log.txt
    if [ $? -ne 0 ];then
        echo "Error Occured with Yum, check yum_log.txt"
        should_continue
        
    ######Success#######
    
    else
        echo "Yum Update succesfull!"
        should_continue
    fi
}

yum_update


####Securing SSH#######

secure_sshdconfig(){
 
 
    echo -e "\e[34m---------------------------------------------------------------------------------------------------------\e[00m"
    echo -e "\e[93m[+]\e[00m Securing SSH Config will do the following:
    echo -e "\e[34m1-Change the SSH Port to 5432e[00m"
    echo -e "\e[34m2-Disable Direct Login as Root.[00m"
    echo -e "\e[34m3-Disallow Empty passwords.[00m"
    
    should_continue
    
    
    #####Making a copy of the current SSH config#######
    /usr/bin/cp /etc/ssh/sshd_config /etc/ssh/sshd_config_original
    
    ######Uploading a custom sshd_config as per best practices#######
    /usr/bin/cat sshd_config>/etc/ssh/sshd_config
    
    #######Restarting SSHd###########
    service sshd restart>sshdstartlog.txt
    service sshd status>>sshdstartlog.txt
    
    ######Error Checks#########
    
    if [ $? -ne 0 ];then
        echo "An Error Occured while securing SSHd, check sshdstartlog.txt"
        echo "Reverting back to original SSHdconfig"
        /usr/bin/cp -rf /etc/ssh/sshd_config_original /etc/ssh/sshd_config
        service sshd restart
        if [ $? -eq 0 ];then
            echo "sshd_confifg was reverted sucessfully"
        fi
        
    else
        echo "SSH was secured sucessfully"
        should_continue
    fi
}    

secure_sshdconfig


########Installing chkrootkit########

install_chkrootkit(){
    

    echo -e "\e[34m---------------------------------------------------------------------------------------------------------\e[00m"
    echo -e "\e[93m[+]\e[00m Installing Chkrootkit"
    echo -e "\e[34m---------------------------------------------------------------------------------------------------------\e[00m"
    
    
    
    #######Install Steps#########
    wget -c ftp://ftp.pangeia.com.br/pub/seg/pac/chkrootkit.tar.gz 2>/dev/null
    tar –xzf chkrootkit.tar.gz 2>/dev/null
    mkdir -p /usr/local/chkrootkit 2>/dev/null
    mv chkrootkit-*/* /usr/local/chkrootkit 2>/dev/null
    cd /usr/local/chkrootkit 2>/dev/null
    
    #####Compile the excutables#####
    make sense > chrootkitinstall.log 
    
    
    #####Error checks########
    if [ $? -ne 0 ];then
        echo "Error Occured with chkrootkit installation, check chrootkitinstall.log"
        should_continue
    
    #######Install success#####
        
    else
        echo "CHKRootkit was installed successfully"
        echo "You can run it using the command:"
        echo -e "\e[93m/usr/local/chkrootkit/chkrootkit\e[00m"
        should_continue
        
}

install_chkrootkit



########Installing Fail2ban########
install_fail2ban(){

    echo -e "\e[34m---------------------------------------------------------------------------------------------------------\e[00m"
    echo -e "\e[93m[+]\e[00m Installing Fail2Ban"
    echo -e "\e[34m---------------------------------------------------------------------------------------------------------\e[00m"
    
    yum -y install fail2ban
    systemctl enable fail2ban
    service fail2ban start
    service fail2ban status>fail2ban_status.log
    
    
    #####Error checks########
    if [ $? -ne 0 ];then
        echo "Error Occured with fail2ban setup, check fail2ban_status.log"
        should_continue
    else
        echo "Fail2ban was installed successfully"
        echo "You can check logs under: /var/log/fail2ban.log"
        should_continue
}

install_fail2ban

########Installing ConfigServerSecurityFirewall (CSF) ########

install_CSF(){


    
    echo -e "\e[34m---------------------------------------------------------------------------------------------------------\e[00m"
    echo -e "\e[93m[+]\e[00m Installing CSF"
    echo -e "\e[34m---------------------------------------------------------------------------------------------------------\e[00m"
    
    #####Installing perl########
    yum -y install perl-libwww-perl 2>/dev/null
    cd /usr/src
    
    #####Downloading CSF tar######
    wget https://download.configserver.com/csf.tgz2>/dev/null
    tar xzf csf.tgz
    cd csf
    
    #####Installation and setup steps######
    sh install.sh
    perl /usr/local/csf/bin/csftest.pl>csftest.log
    
    echo "Please view the results of csftest.log"
    
    should_continue
    
    ######Uploading hardened firewall rules######
    
    /usr/bin/cat csf.conf>/etc/csf/csf.conf
    
    ######Starting CSF#######
    systemctl enable csf
    service csf start>csfstart.log
    csf -e>>csfstart.log
    csf -r>>csfstart.log
    
    #####Error checks########
    if [ $? -eq 0 ];then
        echo "CSF was installed and setup successfully"
        should_continue
    else
        echo "CSF failed to start, check csfstart.log"
        should_continue
    fi
    

    
}

install_sysstat(){

    echo -e "\e[34m---------------------------------------------------------------------------------------------------------\e[00m"
    echo -e "\e[93m[+]\e[00m Installing Sysstat"
    echo -e "\e[34m---------------------------------------------------------------------------------------------------------\e[00m"
    
    yum -y install 
    systemctl enable sysstat
    service sysstat start
    service sysstat status>sysstat_status.log
    if [ $? -ne 0 ];then
        echo "Error Occured with sysstat setup, check sysstat_status.log"
        should_continue
    else
        echo "sysstat was installed successfully\n"
        echo "You can check load averages using:
                -sar -q #For Load average
                -sar -r #memory averages
                -sar -p  #CPU averages"
                
        should_continue
}

install_sysstat


########Making sure unwanted services are disabled ########

disable_unwanted_services(){
    
    echo -e "\e[34m---------------------------------------------------------------------------------------------------------\e[00m"
    echo -e "\e[93m[+]\e[00m Disabling unwanted services"
    echo -e "\e[34m---------------------------------------------------------------------------------------------------------\e[00m"
    
    for service in `cat unwantedservices`;do
        systemctl disable service> 2/dev/null
        systemctl stop service > 2/dev/null
    done
    
    echo -e "\e[34m---------------------------------------------------------------------------------------------------------\e[00m"
    echo -e "\e[93m[+]\e[00m Hardening Process Completed"
    echo -e "\e[34m---------------------------------------------------------------------------------------------------------\e[00m"
}

disable_unwanted_services