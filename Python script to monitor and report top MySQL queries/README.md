**Introduction**:

This is a simple script that reports the top queries in MySQL based on the running time. Query Runing time was chosen as the paramter since long running queries are generally gthat consume higher CPU time and have higher probability of being killed. We are using the pymysql module to achieve the qurery processing

**Working**:

1. We import the pymysql module into our script. The alternative is mysql-connector, however pymysql provides better functionality with filtering output
2. We are storing storing the password in a config.py file and importing the same into our script, to avoid plaintext passwords
3. pymysql connects to the database/mysql-server and performs a query to get longest running queries on the DB/System
4. A cusror is created to interact with the database
5. The cursor returns the query output as a list of dictonaries, with key as column ID and value as value.
6. The user is given a (y/n)choice to kill the longest running query. If y then the longest query is killed, else if n the script exits.
7. Built try-catch-finally blocks to capturte exceptions at each stage
8. We display the output to the user in descending order of longest running queries, and then append-write it to a log file with timestamp entry. 
9. Exception is printed if it occurs
10. The log file can be used for further analyis or can be tailed in real-time as a monitoring solution. 

**Pros/Cons**

* Simple execution, easy to add more functionality by adding additional desired queries
* Log file can be further analyzed with tools like graylog/splunk
* The script can be setup as a cron or run as a background task

**Scope of improvement**:

* Better visual interface

Sample Output:

* https://ibb.co/2vPrnQs

* logfile: https://ibb.co/yXbXTHr


