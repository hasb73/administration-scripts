#Import required modules
import pymysql,subprocess,os,sys,datetime,time
import config

#Declare logfile variable
logfile='mysqlmon.log'
date = datetime.datetime.now().strftime("%d-%m-%Y %H:%M:%S")


#We connect to the database with connect. Password is not passed in plaintext.
#Cursorclass is declared as dictionary for easy filtering
con = pymysql.connect('localhost', 'root', config.password, cursorclass=pymysql.cursors.DictCursor)

#Implementing as try-catch block
try:

    with con.cursor() as cur:

        cur.execute('SELECT * FROM information_schema.processlist ORDER BY TIME DESC')
        
        #fetchall function gets all records of the query as a list of dictonaries with key as column name and value as column value
        rows = cur.fetchall()
        longest= rows[0]['ID']

        print("-------Longest Running queries are--------")
        
        with open(logfile, "a") as log:
            for row in rows:
            
                #Calling dictory values and giving formatted output       
                print(F"\nID:{row['ID']}\nDatabase:{row['DB']}\nCommand:{row['COMMAND']}\nQuery:{row['INFO']},\nRunningTime:{row['TIME']}\n")
                print("--------------------------------------")
                
                Database=row['DB']
                Command=row['COMMAND']
                Query=row['INFO']
                RT=row['TIME']
                
                #Writing to formatted log file with appended timestamp before entry, for further analysis if neeeded
                log.write('TimeStamp:{0}\n Database:{1}\tCommand:{2}\tQuery:{3}\tRunningTime:{4}\n'.format(date,Database,Command,Query,RT))
                
        #Giving the user a choice to kill the longest query
        reply = str(input('Kill the longest running query?: (y/n): ')).lower().strip()
        
        #creating the sql argument to be used as a query by pymysql
        sql = """kill {0};""".format(longest)
        if reply[0] == 'y':
            try:
                cur.execute(sql)
            except pymysql.err.OperationalError as ex:
                print(F"Exception {ex} has occured")
            finally:
                print(F"Query with ID:{longest} was killed sucessfully")
                
        if reply[0] == 'n':  #fixed typo
            print("-----Exiting------")
        
except pymysql.err.ProgrammingError as e:
    print(F"Exception {e} has occured")
    

finally:
    con.close()
    print("-----End of script..Exiting------") 