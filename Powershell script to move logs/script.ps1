function Move-logs {
   
$timestamp=Get-Date -UFormat '%Y%m%d_%H%M'
$path= "C:\inetpub\logs"
$hostname= gc "env:computername"
$remoteserver= Read-Host -Prompt 'Enter remote server hostname or IP'
$DestinationPath=Read-Host -Prompt 'Enter destination path of remote drive'
$createpath="$Destinationpath\$hostname"
$finalpath="$createpath\$timestamp.zip"


       ## Zip up the logs folder
       Compress-Archive -Path $Path -DestinationPath $Path\$timestamp.zip -CompressionLevel Optimal
 
       ## Create a PowerShell Persisteny Session session
       
       $session = New-PSSession -ComputerName $remoteserver -Credential administrator

       #Create a Folder for the current server named as its hostname in the remote drive
       
       $exists=Invoke-Command -Session $session -ScriptBlock { mkdir -Path $Using:createpath -Force }

       #Copy logs to remote drive
       
       Copy-Item -Path $Path\$timestamp.zip -ToSession $session -Destination $createpath

      
       #Verify if Copied zip exists on remote drive
       
       $exists=Invoke-Command -Session $session -ScriptBlock { Test-Path -Path $Using:finalpath }


       if ($exists -eq $false) {write-host "The logs could not be transferred"}



       Else {
       Write-Host "Logs Transferred Sucessfully"
   
       }
   

        #Close session
        Remove-PSSession -Session $session -ErrorAction Ignore
        
        #Clear Zip Files
        Remove-Item $Path\$timestamp.zip -Recurse
    }



 Move-logs 
