**Introduction**

This simple script creates a zip of the logs directory on a local server and transfers it to a remote drive at the user-specified host and path.

**Prerequistes**:

We used 2 Windows EC2 instances. The following steps were performed on the target server:

1. Enabled PS-Remoting from powershell console
2. With "*winrm quickconfig*" we enabled Remote Access Management
3. Added the IP of the source server to the TrustedHosts list via "*Set-Item WSMan:localhost\client\trustedhosts*"
4. Finally opened winrm port inbound port 5985 in AWS security group
5. Tested the connection using "Test-WSMan <Remote IP>"


**Approach**

The script has a function has been broken down in the following steps:

1. The function accepts target server IP/Host and Destination path
2. The script then creates a zip of the specified path in $Path and appends the zip name with the 
   current timestamp
3. The script creates a Persistent Session with the remote server
4. The script will prompt for administrator password of the remote server, 
5. Then it shall create a directory in the Destination Path of the target server, with the name 
   set as the hostname of the source server. The aim is to create better log file segregation if logs 
   from multiple servers are being pushed to the drive
6. The zip file created will be copied to the remote drive under the directory created in step 5
7. Finally the script will check if the zip is present in the remote drive. The script will invoke the Test-Path 
   cmdlet and use the scope "Using" in order to use the local variables
8. As part of the cleanup, the script will close the PSSession and remove the zip from the local drive


**Pros/Cons**:

* The script can be run as a scheduled task.
* A progress bar can be added in future builds


**Output**:

* https://ibb.co/0Bkv3XZ
* https://ibb.co/gjgzFq9