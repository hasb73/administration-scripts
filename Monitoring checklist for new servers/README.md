Hello, the checklist can be obtained from the URL below:

https://docs.google.com/spreadsheets/d/1FaVR9zx-nbvmePXCP8eBtrz3dcFqe9rZ2dDBjAh75is/edit?usp=sharing

There are 2 sheets (windows and linux). The checklist has been prepared with a view of the initial setup and monitoring steps to be taken on a newly provisioned server before it is ready for deployment.