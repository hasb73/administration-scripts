import os, argparse, sys, subprocess

cwd = os.getcwd()
print(f"\n\nWorking directory is {cwd}\n\n")


if os.geteuid() != 0:
    exit("User is not root\tYou need to have root privileges to run this script")
else:
    print("Root user detected, continuing execution\n\n")

#parser = argparse.ArgumentParser(description='Enter the number of older kernels to remove?')
#parser.add_argument('--number', '-n', type=int, help='the number of older kernels to remove')

# proc = subprocess.run(['yum', 'list', 'kernel' '|' 'tr', '-d', 'kernel-'])
# cur_kernel = subprocess.run(['uname','-r'])


##Current Kernel
cur_kernel = subprocess.run(['uname', '-r'], stdout=subprocess.PIPE)

if cur_kernel.returncode != 0:
    print(cur_kernel.stderr)
else:
    print(f"Current kernel is\n{cur_kernel.stdout.decode()}")

##List kernels


list_kernel = subprocess.run(['rpm', '-q', 'kernel'], stdout=subprocess.PIPE)

if list_kernel.returncode != 0:
    print(list_kernel.stderr)
else:
    print(f"List of installed kernels are:\n{list_kernel.stdout.decode()}")

# making list of kernels

ktemp = cur_kernel.stdout.decode()
ktemp = 'kernel-' + ktemp
k = ktemp.split("\n")
lok = list_kernel.stdout.decode().split("\n")

# removing empty strings in list

while ("" in k):
    k.remove("")
while ("" in lok):
    lok.remove("")

# print(k)
# print(lok)

# convert to sets

s1 = set(k)
s2 = set(lok)

# get difference
unused_kernels = s2 - s1

count = str(len(unused_kernels))
print(f"number of unused kernels is:{count}")

# unused_kernels=str(unused_kernels).replace("'", "")

print("\n\n The following kernels are unused")

for i in unused_kernels:
    print(f"--{str(i)}--")

reply = str(input('Remove unused kernels?: (y/n): ')).lower().strip()
if reply[0] == 'y':
    # install yum utils
    yumutils = subprocess.run(['yum', '-y', 'install', 'yum-utils'], stdout=subprocess.PIPE)
    if yumutils.returncode == 0:
        print("yumutils is installed")
        
        

        #package-cleanup --oldkernels --count=2

        logfile = open('output.txt', 'w')
        cleankernels=subprocess.run(["package-cleanup", "--oldkernels", "--count", count], stdout=logfile, stderr=logfile)
        if cleankernels.returncode == 0:
            print('----Script Output----')
            with open('output.txt', 'r') as output:
                print(output.read())

        else:
            print(f"There was an error, refer the log file under {cwd}/out2.txt")

    else:
        print("yumutils failed to install")

if reply[0] == 'n':
    print("exiting script")
    sys.exit([1])