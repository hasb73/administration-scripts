**Introduction** 

This script aims to remove older unused kernels on a centOS server. It is an interactive script that displays list of installed kernels and unused kernels and performs kernel cleanup

**Working**

1. The script imports the required modules, subprocess,os,sys etc.
2. For security checks, it checks for root privileges, exits if the user is not root.
3. Gives the current kernel information and list of installed kernels as list and prints them
4. The user audits the information, and is given a choice to continue. If yes, the script installs the yum- 
   utils package and performs the package-cleanup command on older kernels.
5. The logs(stdout and stderr) of the above command are stored in a logs file in the cwd for the user to 
   audit
6. In case of failure the error is printed and the script terminates with an exit status of 1


**Pros/Cons**

* We have used the package-cleanup functionality of yum rather than manually removing kernels using yum remove kernel to avoid unwanted errors
* The script can be setup a periodic cron job
* The script can be improved in further builds by providing accurate kernel size of unused kernels in human readable format

Sample Output

* https://ibb.co/NZrgBdP






